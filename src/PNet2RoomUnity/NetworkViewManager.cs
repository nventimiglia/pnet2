﻿using System.Collections.Generic;
using PNet;
using UnityEngine;

namespace PNet2RoomUnity
{
    internal class NetworkViewManager
    {
        private readonly Net _net;

        public NetworkViewManager(Net net)
        {
            _net = net;
        }

        readonly Dictionary<NetworkViewId, NetworkView> _networkViews = new Dictionary<NetworkViewId, NetworkView>();

        public IEnumerable<KeyValuePair<NetworkViewId, NetworkView>>  Items
        {
            get { return _networkViews; }
        }

        internal void AddView(PNetR.NetworkView newView, NetworkView view)
        {
            NetworkView existing;
            if (_networkViews.TryGetValue(newView.Id, out existing))
                Object.Destroy(existing);
            _networkViews[newView.Id] = view;
            view.SetNetworkView(_net, newView);
        }

        internal bool TryGetView(NetworkViewId viewId, out NetworkView view)
        {
            return _networkViews.TryGetValue(viewId, out view);
        }

        internal bool Remove(PNetR.NetworkView view)
        {
            NetworkView existing;
            if (_networkViews.TryGetValue(view.Id, out existing) && existing.NetView == view)
                return _networkViews.Remove(view.Id);
            Debug.LogWarning("Attempted to remove view id" + view.Id + ", but it is not the same view");
            return false;
        }

        internal void Clear()
        {
            _networkViews.Clear();
        }
    }
}