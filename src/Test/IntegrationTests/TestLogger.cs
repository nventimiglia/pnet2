﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PNet;

namespace IntegrationTests
{
    /// <summary>
    /// Console recipient for the log
    /// </summary>
    public sealed class TestLogger : ILogger
    {
        public readonly TaskCompletionSource<bool> Tcs = new TaskCompletionSource<bool>();

        private readonly string _name;

        public TestLogger(string name)
        {
            _name = name;
        }

        /// <summary>
        /// Info
        /// </summary>
        /// <param name="info"></param>
        /// <param name="args"></param>
        public void Info(string info, params object[] args)
        {
            Console.WriteLine(DateTime.Now + " " + _name + ": " + info, args);
        }

        /// <summary>
        /// Warning
        /// </summary>
        /// <param name="info"></param>
        /// <param name="args"></param>
        public void Warning(string info, params object[] args)
        {
            Console.WriteLine(DateTime.Now + " " + _name + ": " + info, args);
        }

        /// <summary>
        /// error
        /// </summary>
        /// <param name="info"></param>
        /// <param name="args"></param>
        public void Error(string info, params object[] args)
        {
            var msg = string.Format(DateTime.Now + " " + _name + ": ERROR " + info, args);
            Console.WriteLine(msg);
            Tcs.TrySetException(new Exception(msg));
            Assert.Fail(info, args);
        }

        public void Exception(Exception exception, string info, params object[] args)
        {
            Console.WriteLine(DateTime.Now + " " + _name + ": EXCEPTION {1} : {0}", exception, string.Format(info, args));
            Tcs.TrySetException(exception);
            Assert.Fail(exception + info, args);
        }
    }
}
